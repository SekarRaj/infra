
cp /lib/systemd/system/docker.service -> /etc/systemd/system/docker.service

mkdir ~/.docker
-- Create TLS certificates -> Give proper locations 

systemctl restart docker

[Service]
Type=notify
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock \
  --tlsverify --tlscacert="/home/centos/.docker/ca.pem" \
  --tlscert="/home/centos/.docker/server-cert.pem" \
  --tlskey="/home/centos/.docker/server-key.pem" \
  -H=0.0.0.0:2376
ExecReload=/bin/kill -s HUP $MAINPID
TimeoutSec=0
RestartSec=2
Restart=always