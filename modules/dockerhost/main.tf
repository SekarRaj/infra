resource "aws_instance" "dockerhost" {
  ami                         = "${var.image}"
  associate_public_ip_address = true
  key_name                    = "${var.key_name}"
  instance_type               = "${var.instance_type}"
  subnet_id                   = "${element(var.public_subnet_ids, 2)}"

  vpc_security_group_ids = ["${aws_security_group.dockerhost.id}"]

  tags = {
    Name = "${var.environment}.dockerhost"
  }
}

resource "null_resource" "provisioners" {
  provisioner "remote-exec" {
    inline = [
      "mkdir -p /home/${var.ssh_user}/ansible/playbooks"
    ]

    connection {
      agent       = false
      host        = "${aws_instance.dockerhost.public_ip}"
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file("${path.module}/ansible/${var.private_key_file_name}")}"
      timeout     = "${var.provisioners_timeout}"
    }
  }

  provisioner "file" {
    source      = "${path.module}/ansible/docker-playbook"
    destination = "/home/${var.ssh_user}/ansible/playbooks"

    connection {
      host        = "${aws_instance.dockerhost.public_ip}"
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file("${path.module}/ansible/${var.private_key_file_name}")}"
      timeout     = "${var.provisioners_timeout}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum -y install ansible",
      "sudo pip install --upgrade ansible",
      "cd /home/${var.ssh_user}/ansible/playbooks/docker-playbook; ansible-playbook site.yml",
    ]

    connection {
      host        = "${aws_instance.dockerhost.public_ip}"
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file("${path.module}/ansible/${var.private_key_file_name}")}"
      timeout     = "${var.provisioners_timeout}"
    }
  }

  depends_on = ["aws_instance.dockerhost"]
}


resource "aws_security_group" "dockerhost" {
  name        = "${var.environment}_dockerhost_sg"
  description = "Used in ${var.environment}"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "${var.environment}"
  }
}
