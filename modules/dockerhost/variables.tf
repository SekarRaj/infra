variable "vpc_cidr" {}
variable "image" {
  description = "AMI for the bastion host"
}

variable "environment" {}

variable "ssh_user" {
  description = "EC2 default user"
}

variable "private_key_file_name" {
  default     = "awsadmin.pem"
  description = "Private key for SSH access"
}

variable "key_name" {
  description = "SSH key name"
}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "provisioners_timeout" {
  default     = "1m"
  description = "Default timeout for Terrafrom provisioners"
}

variable "vpc_id" {}

variable "public_subnet_ids" {}

variable "instance_type" {}
