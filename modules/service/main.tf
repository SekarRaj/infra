module "alb" {
  source = "../alb"

  environment       = "${var.environment}"
  alb_name          = "${var.service}-${var.environment}"
  vpc_id            = "${var.vpc_id}"
  public_subnet_ids = "${var.public_subnet_ids}"
}

resource "aws_security_group_rule" "alb_to_ecs" {
  type                     = "ingress"
  from_port                = "${var.container_port}"
  to_port                  = "65535"
  protocol                 = "tcp"
  source_security_group_id = "${module.alb.alb_security_group_id}"
  security_group_id        = "${var.ecs_security_id}"
}

data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}

data "template_file" "template" {
  template = "${file("${path.module}/template/${var.service}.json.tpl")}"
}

resource "aws_ecs_task_definition" "task_definition" {
  family                   = "${var.service}"
  execution_role_arn       = "${data.aws_iam_role.ecs_task_execution_role.arn}"
  network_mode             = "bridge"
  requires_compatibilities = ["EC2"]
  cpu                      = "${var.cpu}"
  memory                   = "${var.memory}"
  container_definitions    = "${data.template_file.template.rendered}"
}

resource "aws_ecs_service" "service" {
  name            = "${var.service}"
  cluster         = "${var.cluster_id}"
  task_definition = "${aws_ecs_task_definition.task_definition.arn}"
  desired_count   = 1
  launch_type     = "EC2"

  # network_configuration {
  #   security_groups  = ["${module.alb.alb_security_group_id}"]
  #   subnets          = "${var.private_subnet_ids}"
  #   assign_public_ip = true
  # }

  load_balancer {
    target_group_arn = "${module.alb.default_alb_target_group}"
    container_name   = "${var.service}"
    container_port   = "${var.container_port}"
  }
}
