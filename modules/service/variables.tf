variable "environment" {
  description = "Name of the environment"
}

variable "cluster_id" {
  description = "Cluster Id to create service"
}

variable "private_subnet_ids" {
  default = "List of private subnet IDs"
}

variable "public_subnet_ids" {
  description = "List of Public subnet IDs"
}

variable "vpc_id" {
  description = "Name of the VPC"
}

variable "service" {
  default     = "sonar"
  description = "Service Name"
}

variable "ingress_from_port" {
  default     = 32768
  description = "Starting port number for ingress"
}

variable "ingress_to_port" {
  default     = 35000
  description = "Ending port number for ingress"
}

variable "cpu" {
  default     = "1024"
  description = "Ending port number for ingress"
}

variable "memory" {
  default     = "2048"
  description = "Ending port number for ingress"
}

variable "template" {
  default     = "data.template_file.sonar.rendered"
  description = "Task definition template"
}

variable "ecs_security_id" {
  description = "ECS Instance security group id"
}

variable "container_port" {
  description = "Port of the container that will be executed"
}

