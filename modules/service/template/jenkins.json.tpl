[
  {
    "name": "jenkins",
    "image": "sekar/jenkins:latest",
    "cpu": 1024,
    "memory": 2048,
    "networkMode": "bridge",
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 0
      },
      {
        "containerPort": 50000,
        "hostPort": 0
      }
    ]
  }
]