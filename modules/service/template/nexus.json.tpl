[
  {
    "name": "nexus",
    "image": "sonatype/nexus3:latest",
    "cpu": 1024,
    "memory": 2048,
    "networkMode": "bridge",
    "portMappings": [
      {
        "containerPort": 8081,
        "hostPort": 0
      }
    ]
  }
]