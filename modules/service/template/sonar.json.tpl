[
  {
    "name": "sonar",
    "image": "owasp/sonarqube:latest",
    "cpu": 1024,
    "memory": 2048,
    "networkMode": "bridge",
    "portMappings": [
      {
        "containerPort": 9000,
        "hostPort": 0
      },
      {
        "containerPort": 9092,
        "hostPort": 0
      }
    ]
  }
]