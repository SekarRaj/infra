variable "vpc_cidr" {}
variable "image" {
  description = "AMI for the bastion host"
}

variable "environment" {

}

variable "key_name" {
  description = "SSH key name"
}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "vpc_id" {}

variable "public_subnet_ids" {}

variable "instance_type" {}
