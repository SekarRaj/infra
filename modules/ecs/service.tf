module "jenkins" {
  source = "./../service"

  cluster_id        = "${module.cluster.cluster_id}"
  service           = "jenkins"
  environment       = "${var.environment}"
  vpc_id            = "${var.vpc_id}"
  public_subnet_ids = "${var.public_subnet_ids}"
  ecs_security_id   = "${module.ecs_instances.ecs_instance_security_group_id}"
  container_port    = "8080"
}

module "sonar" {
  source = "./../service"

  cluster_id        = "${module.cluster.cluster_id}"
  service           = "sonar"
  environment       = "${var.environment}"
  vpc_id            = "${var.vpc_id}"
  public_subnet_ids = "${var.public_subnet_ids}"
  ecs_security_id   = "${module.ecs_instances.ecs_instance_security_group_id}"
  container_port    = "9000"
}

module "nexus" {
  source = "./../service"

  cluster_id        = "${module.cluster.cluster_id}"
  service           = "nexus"
  environment       = "${var.environment}"
  vpc_id            = "${var.vpc_id}"
  public_subnet_ids = "${var.public_subnet_ids}"
  ecs_security_id   = "${module.ecs_instances.ecs_instance_security_group_id}"
  container_port    = "8081"
}
