resource "aws_key_pair" "awsadmin" {
  key_name   = "awsadmin"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDY48KwhT7KGCRX3wgjZon8ukq+sT84pwbdxUT5g6QE88V9ySjFMtqcSxhsqQqkBlMG8PiwRta9GRPz1fLYwaEdIauiV4J5rzfog3nEOXB5G0Yle7IRD7koYQyV68R2XdS/7xYq0lrlPa/x4G3nicI2i2EGT+Uuz3+KiX/NtC07RifIn65CEuNIeYrsFPcvrVG8gCXYh9X3lE74nLntgMHoxqM0OqRGnKkDzZHmLhSrRhetg/2a0fPV3S9qTNWRQUAkhGWVpYQPSKE8nx3h6HlGNRXz6XL5qtBTZvy8B8gRriQCzG1jZ2Wx2VNDPDgX78cafxn6Lpzo9pE6+RmjgKjb"
}

module "network" {
  source = "./modules/network"

  vpc_cidr             = "${var.vpc_cidr}"
  environment          = "${var.environment}"
  public_subnet_cidrs  = "${var.public_subnet_cidrs}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  availability_zones   = "${var.availability_zones}"
}

module "bastion" {
  source               = "./modules/bastion"
  image                = "${lookup(var.bastion_ami, var.region)}"
  key_name             = "${aws_key_pair.awsadmin.key_name}"
  vpc_cidr             = "${var.vpc_cidr}"
  environment          = "${var.environment}"
  public_subnet_cidrs  = "${var.public_subnet_cidrs}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  availability_zones   = "${var.availability_zones}"
  vpc_id               = "${module.network.vpc_id}"
  public_subnet_ids    = "${module.network.public_subnet_ids}"
  instance_type        = "${var.bastion_instance_type}"
}


module "dockerhost" {
  source               = "./modules/dockerhost"
  image                = "${lookup(var.docker_ami, var.region)}"
  key_name             = "${aws_key_pair.awsadmin.key_name}"
  vpc_cidr             = "${var.vpc_cidr}"
  environment          = "${var.environment}"
  public_subnet_cidrs  = "${var.public_subnet_cidrs}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  availability_zones   = "${var.availability_zones}"
  vpc_id               = "${module.network.vpc_id}"
  public_subnet_ids    = "${module.network.public_subnet_ids}"
  instance_type        = "${var.agent_instance_type}"
  ssh_user             = "${var.ssh_user}"
}

module "ecs" {
  source               = "./modules/ecs"
  vpc_cidr             = "${var.vpc_cidr}"
  environment          = "${var.environment}"
  public_subnet_cidrs  = "${var.public_subnet_cidrs}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  availability_zones   = "${var.availability_zones}"
  max_size             = "${var.max_size}"
  min_size             = "${var.min_size}"
  desired_capacity     = "${var.desired_capacity}"
  key_name             = "${aws_key_pair.awsadmin.key_name}"
  instance_type        = "${var.ecs_instance_type}"
  ecs_aws_ami          = "${lookup(var.ecs_aws_ami, var.region)}"
  vpc_id               = "${module.network.vpc_id}"
  private_subnet_ids   = "${module.network.private_subnet_ids}"
  depends_id           = "${module.network.depends_id}"
  public_subnet_ids    = "${module.network.public_subnet_ids}"
}
