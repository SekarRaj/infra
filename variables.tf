variable "region" {
  description = "Default resource creation region"
  default     = "us-east-2"
}

variable "vpc_cidr" {
  description = "CIDR range for VPC"
}
variable "environment" {}
variable "max_size" {}
variable "min_size" {}
variable "desired_capacity" {}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}



//Instance types
variable "ecs_instance_type" {}
variable "agent_instance_type" {}
variable "bastion_instance_type" {}

//ECS Optimized Linux
variable "ecs_aws_ami" {
  type = "map"
  default = {
    us-east-1 = "ami-02507631a9f7bc956"
    us-east-2 = "ami-0329a1fdc914b0c55"
  }
}

//Default - Amazon Linux 2 AMI (HVM), SSD Volume Type
variable "bastion_ami" {
  type = "map"
  default = {
    us-east-1 = "ami-0b898040803850657"
    us-east-2 = "ami-005930c8f6eb929ba"
  }
  description = "Default AMI to use for creating instance"
}

variable "ssh_user" {
  default     = "ec2-user"
  description = "Default login user for ansible playbook"
}


//Default - Red Hat Enterprise Linux 8 (HVM), SSD Volume Type 
variable "docker_ami" {
  type = "map"
  default = {
    us-east-1 = "ami-098bb5d92c8886ca1"
    us-east-2 = "ami-05220ffa0e7fce3d1"
  }
}
