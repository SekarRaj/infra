terraform {
  required_version = "~> 0.12.0"

  required_providers {
    aws      = ">= 2.7.0"
    null     = "~> 2.1"
    template = "~> 2.1"
  }
}

provider "aws" {
  region = "${var.region}"
}
