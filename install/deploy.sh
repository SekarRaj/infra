# aws ecs register-task-definition --cli-input-json file://sonar.json > SONAR_REGISTERED_TASKDEF.json
# SONAR_TASKDEFINITION_ARN=$( < SONAR_REGISTERED_TASKDEF.json jq .taskDefinition.taskDefinitionArn )


# sed "s,@@SONAR_TASKDEFINITION_ARN@@,$SONAR_TASKDEFINITION_ARN," <service-create-sonar.json >SONAR_SERVICE_DEFINITION.json
# aws ecs create-service --cli-input-json file://SONAR_SERVICEDEF.json | tee SONAR_SERVICE.json
aws ecs create-service --cli-input-json file://service-create-sonar.json | tee SONAR_SERVICE.json