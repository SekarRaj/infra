vpc_cidr = "10.0.0.0/16"

environment = "dev"

public_subnet_cidrs = ["10.0.1.0/24", "10.0.2.0/24"]

private_subnet_cidrs = ["10.0.10.0/24", "10.0.11.0/24"]

availability_zones = ["us-east-2a", "us-east-2b"]

max_size = 3

min_size = 2

desired_capacity = 2

ecs_instance_type = "t3.xlarge"

bastion_instance_type = "t3.medium"

agent_instance_type = "t3.xlarge"
