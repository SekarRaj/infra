### Pipeline Infrastructure

Upon executing the `terraform apply -var-file=dev.tfvars` command the following key components will be created.

Note: Use `terraform destroy -var-file=dev.tfvars -auto-approve` to delete all the components.

1) Bation Server
2) ECS Cluster (two t3.large EC2 instance)
3) Jenkins Service (master)
4) Sonar Service
5) Nexus Service
6) EC2 Dockerhost as Jenkins slave

TODO //
1) Including mount point for the services on to EFS
2) Log collection for ECS 
3) Cloudwatch alarm, if required

### Prerequisite

1) Terraform must be installed with the latest version
2) ssh keys should be created (exisitng can be used)
3) `aws configure` should be executed in the system

### Technologies Used

1) AWS
2) Terrafrom
3) Ansible
4) DockerHub
5) aws-cli 

### Additional Set Up
1) Generate TLS certificates in the hocker host
2) Add docker agent to execution in Jenkins slave
3) Create webhook in sonar
4) Set up SSH port forwarding to connect with Private instances

### Setting up SSH Port forwaring
    EC2 instances created as part of the ECS cluster are created with private IP and host name only. Configuring
and troubleshooting those instance would need bastion server.

### Sonar
Sonar webhook has to be created to enable sonar scanner to send reports to back to Jenkins. Use the below format.

`<Jenkins_URL>/sonarqube-webhook/` 

Default user is `admin` and password is `admin`.

### Nexus 
Default user name is `admin` and the password is generated at `nexus/data`. We need to login to that container to access that data.